<?php
// Kết nối đến cơ sở dữ liệu
$servername = "localhost";
$username = "your_username";
$password = "your_password";
$dbname = "score_management";

$conn = new mysqli($servername, $username, $password, $dbname);

// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Lấy tham số từ URL
$sinh_vien = $_GET['sinh_vien'];
$mon_hoc = $_GET['mon_hoc'];

// Xây dựng câu truy vấn dựa trên tham số
$sql = "SELECT scores.id, students.name AS sinh_vien, teachers.name AS giao_vien, subjects.name AS mon_hoc, scores.score, scores.description
        FROM scores
        INNER JOIN students ON scores.student_id = students.id
        INNER JOIN teachers ON scores.teacher_id = teachers.id
        INNER JOIN subjects ON scores.subject_id = subjects.id";

if (!empty($sinh_vien)) {
    $sql .= " WHERE students.name = '$sinh_vien'";
} elseif (!empty($mon_hoc)) {
    $sql .= " WHERE subjects.name = '$mon_hoc'";
}

// Thực hiện câu truy vấn
$result = $conn->query($sql);

// Trả về kết quả dưới dạng JSON
if ($result->num_rows > 0) {
    $diem = array();

    while ($row = $result->fetch_assoc()) {
        $diem[] = $row;
    }

    echo json_encode(["diem" => $diem]);
} else {
    echo json_encode(["diem" => []]);
}

// Đóng kết nối
$conn->close();
?>

