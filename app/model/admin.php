<?php
// app/model/admin.php

class AdminModel {
    private $db;

    // Constructor to initialize the database connection
    public function __construct($db) {
        $this->db = $db;
    }

    // Example function to get all admins from the database
    public function getAllAdmins() {
        $query = "SELECT * FROM admins";
        $result = $this->db->query($query);

        // Check for query success
        if ($result) {
            $admins = $result->fetch_all(MYSQLI_ASSOC);
            $result->free_result();
            return $admins;
        } else {
            return false;
        }
    }

    // Example function to add a new admin to the database
    public function addAdmin($username, $password) {
        // You should use prepared statements to prevent SQL injection
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $query = "INSERT INTO admins (username, password) VALUES ('$username', '$hashedPassword')";
        $result = $this->db->query($query);

        // Check for query success
        return $result;
    }

    // Example function to authenticate an admin login
    public function authenticateAdmin($username, $password) {
        $query = "SELECT * FROM admins WHERE username = '$username'";
        $result = $this->db->query($query);

        // Check if the username exists
        if ($result && $result->num_rows > 0) {
            $admin = $result->fetch_assoc();

            // Verify the password
            if (password_verify($password, $admin['password'])) {
                return true; // Authentication successful
            }
        }

        return false; // Authentication failed
    }

    // Other admin-related functions can be added as needed
}
?>

