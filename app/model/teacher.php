<?php
// app/model/teacher.php

class TeacherModel {
    private $db;

    // Constructor to initialize the database connection
    public function __construct($db) {
        $this->db = $db;
    }

    // Example function to get all teachers from the database
    public function getAllTeachers() {
        $query = "SELECT * FROM teachers";
        $result = $this->db->query($query);

        // Check for query success
        if ($result) {
            $teachers = $result->fetch_all(MYSQLI_ASSOC);
            $result->free_result();
            return $teachers;
        } else {
            return false;
        }
    }

    // Example function to add a new teacher to the database
    public function addTeacher($name, $email) {
        // You should use prepared statements to prevent SQL injection

        $query = "INSERT INTO teachers (name, email) VALUES ('$name', '$email')";
        $result = $this->db->query($query);

        // Check for query success
        return $result;
    }

    // Example function to edit an existing teacher in the database
    public function editTeacher($teacher_id, $name, $email) {
        // You should use prepared statements to prevent SQL injection

        $query = "UPDATE teachers SET name = '$name', email = '$email' WHERE id = '$teacher_id'";
        $result = $this->db->query($query);

        // Check for query success
        return $result;
    }

    // Example function to delete an existing teacher from the database
    public function deleteTeacher($teacher_id) {
        // You should use prepared statements to prevent SQL injection

        $query = "DELETE FROM teachers WHERE id = '$teacher_id'";
        $result = $this->db->query($query);

        // Check for query success
        return $result;
    }

    // Other teacher-related functions can be added as needed
}
?>

