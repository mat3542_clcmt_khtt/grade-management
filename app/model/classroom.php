<?php
// app/model/classroom.php

class ClassroomModel {
    private $db;

    // Constructor to initialize the database connection
    public function __construct($db) {
        $this->db = $db;
    }

    // Example function to get all classrooms from the database
    public function getAllClassrooms() {
        $query = "SELECT * FROM classrooms";
        $result = $this->db->query($query);

        // Check for query success
        if ($result) {
            $classrooms = $result->fetch_all(MYSQLI_ASSOC);
            $result->free_result();
            return $classrooms;
        } else {
            return false;
        }
    }

    // Example function to add a new classroom to the database
    public function addClassroom($name, $capacity) {
        // You should use prepared statements to prevent SQL injection

        $query = "INSERT INTO classrooms (name, capacity) VALUES ('$name', '$capacity')";
        $result = $this->db->query($query);

        // Check for query success
        return $result;
    }

    // Example function to edit an existing classroom in the database
    public function editClassroom($classroom_id, $name, $capacity) {
        // You should use prepared statements to prevent SQL injection

        $query = "UPDATE classrooms SET name = '$name', capacity = '$capacity' WHERE id = '$classroom_id'";
        $result = $this->db->query($query);

        // Check for query success
        return $result;
    }

    // Example function to delete an existing classroom from the database
    public function deleteClassroom($classroom_id) {
        // You should use prepared statements to prevent SQL injection

        $query = "DELETE FROM classrooms WHERE id = '$classroom_id'";
        $result = $this->db->query($query);

        // Check for query success
        return $result;
    }

    // Other classroom-related functions can be added as needed
}
?>

