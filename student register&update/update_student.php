<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Chỉnh sửa thông tin</title>
</head>

<?php
// Kết nối CSDL
require 'database.php';

global $connect;
// Lấy sinh viên từ Table students
$id = $_POST['id'];
$sql = "SELECT * FROM students WHERE id = $id";
$statement = $connect -> prepare($sql);
$statement -> execute();
$student = $statement -> fetch();


?>

<body>
<div class="container">
    <!-- Form đăng ký với phương thức post tới confirm.php để hiển thị thông tin đăng ký -->
    <form id="registrationForm" class="reg-form" method="POST" action="update.php" enctype="multipart/form-data">

        <!-- ID trong table -->
        <input type="hidden" name="id" value="<?php echo $student['id']; ?>">
        <?php
        date_default_timezone_set('UTC');  // Đặt múi giờ mặc định là UTC
        $currentTime = new DateTime('now', new DateTimeZone('UTC'));
        $currentTime->setTimezone(new DateTimeZone('Asia/Bangkok'));  // Chuyển đổi sang múi giờ Asia/Bangkok (UTC+7)

        $formattedTime = $currentTime->format('Y-m-d H:i:s');

        // In giờ theo múi giờ Asia/Bangkok

        ?>

        <input type="hidden" name="date" value="<?php echo $formattedTime; ?>">
        <!-- Hiển thị lỗi trong việc điền đơn đăng ký -->
        <div id="errorMessages" class="error-messages">
            <script>
                $(document).ready(function () {
                    $("#register_btn").click(function (event) {
                        let name = $("#name").val();

                        let descr = $("#address").val();

                        // Biến flag để check xem có error messages về việc đăng không
                        let flag = true;

                        let errorMessages = [];

                        if (name === "") {
                            errorMessages.push("Hãy nhập tên sinh viên.");
                            flag = false
                        }
                        if (name.length > 100) {
                            errorMessages.push("Không nhập quá 100 ký tự");
                            flag = false
                        }


                        if (descr === "") {
                            errorMessages.push("Hãy nhập mô tả chi tiết.");
                            flag = false
                        }
                        if (descr.length > 100) {
                            errorMessages.push("Không nhập quá 100 ký tự");
                            flag = false
                        }
                        if (!flag) {
                            // Với việc flag = flase tức là có thông tin điền vào không như mong muốn, thì nút "đăng ký" sẽ không thực hiện action
                            event.preventDefault();
                            // Tổng hợp và hiện thông báo lỗi
                            let errorMessageHtml = "";
                            for (let i = 0; i < errorMessages.length; i++) {
                                errorMessageHtml += errorMessages[i] + "<br>";
                            }
                            $("#errorMessages").html(errorMessageHtml);
                        } else {
                            // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
                            $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
                        }
                    });

                    // Đồng bộ tên file khi người dùng chọn ảnh mới
                });
            </script>
        </div>

        <!-- Mục Họ và tên -->
        <div class="form-group">
            <div class="full-name-box required-label"><label for="name">Họ và tên</label></div>
            <input class="full-name-input" type="text" id="name" name="name" value="<?php echo $student['name']; ?>">
        </div>


        <!-- Mục địa chỉ -->
        <div class="form-group-2">
            <div class="address-box"><label for="address">Mô tả thêm</label></div>
            <textarea class="address-input" id="address" name="address"><?php echo $student['description']; ?></textarea>
        </div>

        <!-- Mục Hình ảnh -->

  <div class="form-group" style="align-items: unset">
  <div class="image-box" style="height: 20%"><label for="image">Hình ảnh</label></div>
      <div class="show-image">
          <?php if (!empty($student['avatar'])): ?>
              <?php
              $imagePath = './web/avatar/' . $student['avatar'];
              ?>
              <img src="<?php echo $imagePath ?>" style="width:100%" alt="Hình ảnh của sinh viên">
          <?php else: ?>
              <!-- Hình ảnh mặc định hoặc hình ảnh trống -->
              <img src="./uploads/default.png" style="width:100%" alt="Hình ảnh mặc định">
          <?php endif; ?>

  <div class = "image-input">
  <input type="file" id="image" name="profileImage" accept="image/*" style="display: none;">
  </div>
    <script>
    const currentImg = document.querySelector('.show-image img');
    const fileInput = document.getElementById('image');

    currentImg.addEventListener('click', () => {
    fileInput.click();
    });

    fileInput.addEventListener('change', () => {
    const file = fileInput.files[0];
    const reader = new FileReader();

    const maxFileSize = 1000000; // 1000KB
    const allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
    const imageFileType = file.name.split('.').pop().toLowerCase();

    if (file.size > maxFileSize) {
    alert('Kích thước của ảnh quá lớn');
    return;
    }

    if (!allowedExtensions.includes(imageFileType)) {
    alert('Sai định dạng của ảnh');
    return;
    }

    reader.onload = function(e) {
    currentImg.src = e.target.result;
    };

    reader.readAsDataURL(file);
    });
    </script>

      <div class="signup-button" id="register_btn">
          <button type="submit">Xác nhận</button>
      </div>

    </form>
</div>
</body>

</html>