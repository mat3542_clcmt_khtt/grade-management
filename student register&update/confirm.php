<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Xác nhận thông tin đăng ký</title>
    <script>
        function redirectToRegister() {
            // Thay đổi giá trị của action để không gửi đến insertInformation.php
            document.getElementById("registrationForm").action = "register.php";
            // Gửi form
            document.getElementById("registrationForm").submit();
        }
    </script>
</head>



<body>
    <div class="container">
        <!-- Khi đã xác nhận lại thông tin ta sẽ thực hiện connect và insert data vào Database ở bên database.php -->
        <form id="registrationForm" class="reg-form"  method="POST" action = "insertInformation.php" enctype="multipart/form-data">
            <!-- Hiển thị Họ và tên trong form đăng ký  -->
            <div class="form-group">
                <div class="full-name-box required-label"><label for="name">Họ và tên</label></div>
                <div class="show-name">
                    <?php
                    echo $_POST['name'];
                    ?>
                </div>
                <!-- trường input này là lưu giữ giá trị người dùng nhập cho trường "Họ và tên" khi gửi form, dùng để gửi qua các trang tiếp theo mà không hiển thị-->
                <input type = "hidden" name = "name" value = "<?php echo $_POST['name'];?>">
                <?php
                date_default_timezone_set('UTC');  // Đặt múi giờ mặc định là UTC
                $currentTime = new DateTime('now', new DateTimeZone('UTC'));
                $currentTime->setTimezone(new DateTimeZone('Asia/Bangkok'));  // Chuyển đổi sang múi giờ Asia/Bangkok (UTC+7)

                $formattedTime = $currentTime->format('Y-m-d H:i:s');

                // In giờ theo múi giờ Asia/Bangkok

                ?>

                <input type="hidden" name="date" value="<?php echo $formattedTime; ?>">
            </div>
            <!-- Hiển thị Hình ảnh (nếu có)  -->
            <div class="form-group" style="align-items: unset">
                <div class="image-box" style="height: 20%"><label for="image"></label>Hình ảnh</div>
                <div class="show-image">
                    <?php
                    if (isset($_FILES['profileImage'])) {
                        if ($_FILES['profileImage']['error'] == 0) {
                            $target_dir = './web/avatar/';
                            // Trong trường hợp chưa có thư mục thì ta tiến hành khởi tạo
                            if (!file_exists($target_dir)) {
                                mkdir($target_dir, 0777, true);
                            }
                            $target_file = $target_dir . basename($_FILES['profileImage']['name']);
                            $uploads = true;
                            $maxFileSize = 1000000; // 1000KB
                            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

                            // Kiểm tra Size của ảnh
                            if ($_FILES['profileImage']['size'] > $maxFileSize) {
                                echo '<div>Kích thước của ảnh quá lớn</div>';
                                $uploads = false;
                            }

                            // Kiểm tra định dạng của ảnh
                            if($imageFileType != "jpg"
                                && $imageFileType != "png"
                                && $imageFileType != "jpeg"
                                && $imageFileType != "gif" ) {
                                echo '<div>Sai định dạng của ảnh</div>';
                                $uploads = false;
                            }

                            if ($uploads) {
                                if (move_uploaded_file($_FILES['profileImage']['tmp_name'], $target_file)) {
                                    echo '<img src="./web/avatar/' . $_FILES["profileImage"]['name'] . '" style="width:100%">';
                                } else {
                                    echo 'Không tải lên được ảnh';
                                }
                            }
                        }
                    }
                    ?>
                </div>
                <!-- Với image, ta sẽ lưu trữ đường dẫn (path) của ảnh (trong thư mục uploads) cho giá trị cột image trong database, nếu không có thì sẽ là rỗng -->
                <?php
                $imagePath = '';
                if(!empty($_FILES["profileImage"]["name"])) {
                    $imagePath =  $_FILES["profileImage"]["name"];
                }
                ?>
                <input type="hidden" name="image" value="<?php echo $imagePath; ?>">
            </div>
            <!-- Hiển thị mô tả trong form đăng ký  -->
            <div class="form-group">
                <div class="address-box"><label for="address"></label>Địa chỉ</div>
                <div class="show-address">
                    <?php
                    echo $_POST['address'];
                    ?>
                </div>
                <input type = "hidden" name = "address" value = "<?php echo $_POST['address'];?>">
            </div>



            <!-- Nút Xác nhận  -->
            <div class="submit-button">
                <button type="button" onclick="redirectToRegister()">Sửa lại</button>
                <button type="submit">Đăng ký</button>

            </div>
        </form>



    </div>

</body>

</html>




