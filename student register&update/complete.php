<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Đăng ký tân sinh viên</title>
</head>

<body>
<div class="container">
    <!-- Form đăng ký với phương thức post tới confirm.php để hiển thị thông tin đăng ký -->
    <form id="registrationForm" class="reg-form-2" >
        <!-- Hiển thị thông báo thành công -->
        <div class="success-message">Bạn đã đăng ký thành công</div>

        <!-- Liên kết để trở về trang chủ -->
        <div class="back-to-home"><a href="register.php">Trở về trang chủ</a></div>

    </form>
</div>

</body>



</html>
