<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Đăng ký tân sinh viên</title>
</head>

<body>
<div class="container">
    <!-- Form đăng ký với phương thức post tới confirm.php để hiển thị thông tin đăng ký -->
    <form id="registrationForm" class="reg-form" method="POST" action="confirm.php" enctype="multipart/form-data">
        <!-- Hiển thị lỗi trong việc điền đơn đăng ký -->
        <div id="errorMessages" class="error-messages">
            <script>
                $(document).ready(function () {
                    $("#register_btn").click(function (event) {
                        let name = $("#name").val();
                        let image = $("#image").val();
                        let descr = $("#address").val();

                        // Biến flag để check xem có error messages về việc đăng không
                        let flag = true;

                        let errorMessages = [];

                        if (name === "") {
                            errorMessages.push("Hãy nhập tên sinh viên.");
                            flag = false
                        }
                        if (name.length > 100) {
                            errorMessages.push("Không nhập quá 100 ký tự");
                            flag = false
                        }

                        if (image === "") {
                            errorMessages.push("Hãy chọn avatar");
                            flag = false
                        }

                        if (descr === "") {
                            errorMessages.push("Hãy nhập mô tả chi tiết.");
                            flag = false
                        }
                        if (descr.length > 100) {
                            errorMessages.push("Không nhập quá 100 ký tự");
                            flag = false
                        }
                        if (!flag) {
                            // Với việc flag = flase tức là có thông tin điền vào không như mong muốn, thì nút "đăng ký" sẽ không thực hiện action
                            event.preventDefault();
                            // Tổng hợp và hiện thông báo lỗi
                            let errorMessageHtml = "";
                            for (let i = 0; i < errorMessages.length; i++) {
                                errorMessageHtml += errorMessages[i] + "<br>";
                            }
                            $("#errorMessages").html(errorMessageHtml);
                        } else {
                            // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
                            $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
                        }
                    });

                    // Đồng bộ tên file khi người dùng chọn ảnh mới
                });
            </script>
        </div>
        <!-- Mục Họ và tên -->
        <div class="form-group">
            <div class="full-name-box required-label"><label for="name">Họ và tên</label></div>
            <input class="full-name-input" type="text" id="name" name="name" value="<?php echo isset($_POST['name']) ? $_POST['name'] : ''; ?>" required>
        </div>

        <!-- Mục Avatar -->
        <div class="form-group" id="avt">
            <div class="image-box"><label for="image">Avatar</label></div>
            <div id="imageFileName"><?php echo isset($_FILES['profileImage']['name']) ? $_FILES['profileImage']['name'] : 'Chưa chọn ảnh'; ?></div>
            <input class="image-select" type="file" id="image" name="profileImage" accept="image/*">

        </div>

        <!-- Mục mô tả -->
        <div class="form-group-2">
            <div class="address-box"><label for="address">Mô tả thêm</label></div>
            <textarea class="address-input" id="address" name="address"><?php echo isset($_POST['address']) ? $_POST['address'] : ''; ?></textarea>
        </div>

        <div class="signup-button" id="register_btn">
            <button type="submit">Xác Nhận</button>
        </div>
    </form>
</div>
<script>
    $(document).ready(function () {
        // Đồng bộ tên file khi người dùng chọn ảnh mới
        $("#image").change(function () {
            let fileName = $(this).val().split("\\").pop();
            $("#imageFileName").text(fileName);
        });
    });
</script>
</body>



</html>