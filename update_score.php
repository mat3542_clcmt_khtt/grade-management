<?php
require 'database.php';
global $connect;
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $id = $_POST['id'];
    $name = $_POST['name'];
    $descr = $_POST['address'];
    $date = $_POST['date'];
    $image_path = "";

    // Your existing code for handling image upload
    if (isset($_FILES['profileImage']) && !empty($_FILES['profileImage']['name'])) {

        if ($_FILES['profileImage']['error'] == 0) {
            $target_dir = './web/avatar';
            $target_file = $target_dir . basename($_FILES['profileImage']['name']);
            if(!file_exists($target_file)){
                if (move_uploaded_file($_FILES['profileImage']['tmp_name'], $target_file)) {
                } else {
                    exit('<script>alert("An error occurred while copying the file.");</script>');
                }
            }
        }
        $image_path = $_FILES['profileImage']['name'];
    }


    // Update student information in the students table
    $updateStudent = "UPDATE students
               SET
                   name = '$name',
                   description = '$descr',
                   updated = '$date'
               WHERE id = $id";
    
    if(!empty($image_path)){
        $updateStudent = "UPDATE students
               SET
                   name = '$name',
                   description = '$descr',
                   avatar = '$image_path',
                   updated = '$date'
               WHERE id = $id";
    }

    // Execute the update query for students
    $statementStudent = $connect -> prepare($updateStudent);
    $statementStudent -> execute();

    // Update or insert grades for the student
    $subject = $_POST['subject'];
    $score = $_POST['score'];

    $checkGrade = "SELECT * FROM grades WHERE student_id = $id AND subject = '$subject'";
    $statementCheckGrade = $connect -> prepare($checkGrade);
    $statementCheckGrade -> execute();

    if($statementCheckGrade->rowCount() > 0) {
        // Update the existing grade
        $updateGrade = "UPDATE grades
                   SET
                       score = $score
                   WHERE student_id = $id AND subject = '$subject'";
    } else {
        // Insert a new grade
        $updateGrade = "INSERT INTO grades (student_id, subject, score) VALUES ($id, '$subject', $score)";
    }

    // Execute the update or insert query for grades
    $statementGrade = $connect -> prepare($updateGrade);
    $statementGrade -> execute();

    // Redirect back to the HTML page
    header("Location: complete.php");
    exit;
}
?>

